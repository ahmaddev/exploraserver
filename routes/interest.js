var express = require("express");
var interestController = require("../app/controllers/interest.controller");
var router = express.Router();

router.post("/addinterest", interestController.saveInterest);
// router.get("/listing", interestController.getInterests);
router.get("/getinterest", interestController.gettingId);
router.get("/deleteinterest/:id", interestController.deleteInterest);

module.exports = router;
