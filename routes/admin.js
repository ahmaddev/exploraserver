var express = require("express");
var adminController = require("../app/controllers/admin.controller");
var router = express.Router();

router.post("/adminlogin", adminController.adminLogin);
router.post("/registeradmin", adminController.registerAdmin);

module.exports = router;
