var express = require("express");
var activityController = require("../app/controllers/activity.controller");
var router = express.Router();

router.post("/addactivity", activityController.addactivity);
router.get("/gettingactivities/:id", activityController.getActivities);
router.delete("/deleteactivity/:id", activityController.deleteActivity);
router.get("/addignorantuser/:id", activityController.addIgnorantUser);
// router.get("getmyrequestes/:id", activityController.getMyRequests);
router.get(
  "/getsingleuseractivities/:id",
  activityController.getSingleUserActivity
);
router.get("/applyforactivity/:aid/:uid", activityController.applyForActivity);
router.get("/getsingleactivity/:id", activityController.getSingleActivity);
router.get(
  "/getmyrequestedactivities/:id",
  activityController.getMyRequestedActivities
);

// // adding the activities
// router.post("/addactivity", activityController.addactivity);
// // getting activity feeds
// router.get("/gettingactivities/:id", activityController.getActivities); //
// // deleting acvitiy by the user who created it
// router.delete("/deleteactivity/:id", activityController.deleteActivity);
// // adding ignorant user to the activity
// router.get("/addignorantuser/:aid/:uid", activityController.addIgnorantUser);
// // getting activities of the single user who is logged in
// router.get(
//   "/getsingleuseractivities/:id",
//   activityController.getSingleUserActivity
// );
// // applying for the activity
// router.get("/applyforactivity/:aid/:uid", activityController.applyForActivity);
// // getting the single activity by the activty id
// router.get("/getsingleactivity/:id", activityController.getSingleActivity);
// // getting activities i requested for
// router.get(
//   "/getmyrequestedactivities/:id",
//   activityController.getMyRequestedActivities
// );

module.exports = router;
