var express = require("express");
var userController = require("../app/controllers/user.controller");
var router = express.Router();

/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

router.post("/authenticate", userController.authenticate);
router.post("/register", userController.register);
router.get("/getusers", userController.getUsers);
router.put("/editpersonalinfo/:id", userController.editSingleUser);
router.post("/adduserinterest/:id", userController.addUserInterest);
router.put("/adduserimages/:id", userController.addUserImages);

module.exports = router;
