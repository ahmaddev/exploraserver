var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");
var bcrypt = require("bcryptjs");
const saltRounds = 10;
var Schema = mongoose.Schema;
var UserSchema = new Schema(
  {
    name: { type: String, trim: true },
    location: { type: String },
    city: { type: Array },
    bio: { type: String, default: "" },
    email: { type: String, unique: true, lowercase: true },
    password: { type: String, trim: true },
    rating: { type: Number, default: 0, trim: true },
    token: { type: String, trim: true },
    isEnabled: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false },
    emailverified: { type: Boolean, default: false },
    profileImage: [{ type: String, default: "" }],
    gId: { type: String, trim: true },
    fId: { type: String, trim: true },
    interests: { type: Array }
  },
  { timestamps: true },
  { collection: "user" }
);
UserSchema.plugin(uniqueValidator, { type: "mongoose-unique-validator" });
UserSchema.pre("save", function(next) {
  var user = this;
  if (!user.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(user.password, salt, function(err2, hash) {
      user.password = hash;
      next();
    });
  });
});
module.exports = mongoose.model("user", UserSchema);
