var mongoose = require("mongoose");
var bcrypt = require("bcryptjs");
const saltRound = 10;
var Schema = mongoose.Schema;
var AdminSchema = new Schema({
  email: { type: String, trim: true },
  password: { type: String, trim: true },
  token: { type: String }
});

AdminSchema.pre("save", function(next) {
  var user = this;
  if (!user.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(saltRound, function(err, salt) {
    bcrypt.hash(user.password, salt, function(err2, hash) {
      user.password = hash;
      next();
    });
  });
});

module.exports = mongoose.model("admin", AdminSchema);
