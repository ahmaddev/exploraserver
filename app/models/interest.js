var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");
var Schema = mongoose.Schema;
var InterestSchema = new Schema(
  {
    interestName: { type: String, trim: true }
  },
  { timestamps: true },
  { collection: "interest" }
);
InterestSchema.plugin(uniqueValidator);
module.exports = mongoose.model("interest", InterestSchema);
