var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");
var Schema = mongoose.Schema;
var ActivitySchema = new Schema(
  {
    activity: { type: String, trim: true },
    activityData: { type: Date, default: Date.now() },
    lookingFor: { type: String },
    location: { type: String },
    about: { type: String },
    userId: { type: Schema.Types.ObjectId, ref: "user" },
    ignoreApplicants: [
      {
        userId: Schema.Types.ObjectId
      }
    ],
    activityStatus: {
      type: String,
      enum: ["ACCEPTED", "REJETED", "PENDING"],
      default: "PENDING"
    },
    applicants: [
      {
        userId: { type: Schema.Types.ObjectId, ref: "user" },
        status: {
          type: String,
          enum: ["ACCEPTED", "REJETED", "PENDING"],
          default: "PENDING"
        }
      }
    ],
    isEnabled: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false }
  },
  { timestamps: true },
  { collection: "activity" }
);
ActivitySchema.plugin(uniqueValidator);
module.exports = mongoose.model("activity", ActivitySchema);
