var activityModel = require("../models/activity");
var mongoose = require("mongoose");
const addactivity = (req, res) => {
  req.checkBody("activity", "please send activity ").exists();
  req.checkBody("activityDate", "please send activity Date ").exists();
  req.checkBody("lookingFor", "please send looking for ").exists();
  req.checkBody("about", "please send about").exists();
  var act = new activityModel();
  act.activity = req.body.activity ? req.body.activity : "";
  act.activityDate = req.body.activityDate;
  act.location = req.body.location ? req.body.location : "";
  act.about = req.body.about ? req.body.about : "";
  act.userId = req.body.userId;
  act.save((err, data) => {
    if (err) throw err;
    // else {
    res.json({ status: 200, message: "saved activity successfully" });
    // }
  });
};

const getActivities = (req, res) => {
  var id = req.params.id ? req.params.id : "";
  if (id) {
    id = mongoose.Types.ObjectId(id);
    activityModel
      .find({
        isDeleted: false,
        userId: { $ne: id },
        "ignoreApplicants.userId": { $nin: [id] },
        "applicants.userId": { $nin: [id] }
      })
      .sort({ createdAt: -1 })
      .exec((err, data) => {
        if (err) throw err;
        else if (data) {
          res.json({ status: 200, message: "getting activities", data });
        } else {
          res.json({ status: 201, message: "No Data" });
        }
      });
  } else {
    res.status(401).json({ status: 401, message: "please send id" });
  }
};
const deleteActivity = (req, res) => {
  var id = req.params.id ? req.params.id : "";

  if (id) {
    id = mongoose.Types.ObjectId(id);
    activityModel
      .findOneAndUpdate({ _id: id }, { isDeleted: true })
      .lean()
      .exec((err, data) => {
        if (err) throw err;
        if (data.isModified != 0) {
          res
            .status(200)
            .json({ status: 200, message: "Deleted Successfully" });
        } else {
          res.status(202).json({ status: 202, message: "Error! not updated" });
        }
      });
  } else {
    res.status(401).json({ status: 401, message: "please send id" });
  }
};

const addIgnorantUser = (req, res) => {
  var aid = req.params.aid ? req.params.aid : "";
  var uid = req.params.uid ? req.params.uid : "";
  if (aid && uid) {
    aid = mongoose.Types.ObjectId(aid);
    uid = mongoose.Types.ObjectId(uid);
    var idjson = {
      userId: uid
    };
    activityModel
      .findOneAndUpdate({ _id: aid }, { $push: { ignoreApplicants: idjson } })
      .exec((err, data) => {
        if (err) throw err;
        res
          .status(200)
          .json({ status: 200, message: "User added to ignorant list" });
      });
  } else {
    res.status(403).json({ message: "Please send activity id" });
  }
};
const getSingleUserActivity = (req, res) => {
  var id = req.params.id ? req.params.id : "";

  console.log("GETTING ID", id);
  if (id) {
    id = mongoose.Types.ObjectId(id);
    activityModel
      .find({ userId: id, isDeleted: false })
      .sort({ createdAt: -1 })
      .exec((err, data) => {
        if (err) throw err;
        console.log("MY ACTIVITIES", data);
        res.status(200).json({ status: 200, message: "Activity ", data });
      });
  } else {
    res.status(401).json({ status: 401, message: "please send id" });
  }
};
const applyForActivity = (req, res) => {
  var aid = req.params.aid ? req.params.aid : "";
  var uid = req.params.uid ? req.params.uid : "";
  console.log("YOU HAVE GOT IDs", aid, uid);
  if (aid && uid) {
    aid = mongoose.Types.ObjectId(aid);
    uid = mongoose.Types.ObjectId(uid);
    var idjson = {
      userId: uid
    };
    activityModel
      .findOneAndUpdate({ _id: aid }, { $push: { applicants: idjson } })
      .exec((err, data) => {
        if (err) {
          console.log(err);
          throw err;
        }
        res.json({ status: 200, message: "Added applicant", data });
      });
  } else {
    console.log("You havent got the id");
    res.status(401).json({ status: 401, message: "Please send the id" });
  }
};
const getSingleActivity = (req, res) => {
  var id = req.params.id ? req.params.id : "";
  if (id) {
    id = mongoose.Types.ObjectId(id);
    activityModel
      .find(id)
      .populate({
        path: "applicants.userId",
        select: { name: 1, location: 1, email: 1, rating: 1 }
      })
      .exec((err, data) => {
        if (err) throw err;
        console.log("this is the data&&&&&&&&", data);
        res
          .status(200)
          .json({ status: 200, message: "getting activoty is the ", data });
      });
  } else {
    res
      .status(401)
      .json({ status: 401, message: "please provide with the id" });
  }
};

const getMyRequestedActivities = (req, res) => {
  var id = req.params.id ? req.params.id : "";
  if (id) {
    id = mongoose.Types.ObjectId(id);
    activityModel
      .find({ "applicants.userId": { $in: [id] } })
      .exec((err, data) => {
        if (err) throw err;
        console.log("this is the data&&&&&&&&", data);
        res
          .status(200)
          .json({ status: 200, message: "getting activoty is the ", data });
      });
  } else {
    res
      .status(401)
      .json({ status: 401, message: "please provide with the id" });
  }
};

module.exports = {
  addactivity: addactivity,
  getActivities: getActivities,
  deleteActivity: deleteActivity,
  addIgnorantUser: addIgnorantUser,
  getSingleUserActivity: getSingleUserActivity,
  applyForActivity: applyForActivity,
  getSingleActivity: getSingleActivity,
  getMyRequestedActivities: getMyRequestedActivities
  // getSingleUserActivity: getSingleUserActivity,
  // applyForActivity: applyForActivity,
  // getSingleActivity: getSingleActivity,
  // getMyRequestedActivities: getMyRequestedActivities
};
