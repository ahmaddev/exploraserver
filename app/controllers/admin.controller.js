var adminModel = require("../models/admin");
var bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");

var adminLogin = (req, res) => {
  req.checkBody("email", "Please send email").exists();
  req.checkBody("password", "Please send password ").exists();
  var errors = req.validationErrors();
  if (errors) {
    console.log("getting errors" + JSON.stringify(errors));
    res.status(400).json({ errors: errors });
  }
  var admin = {};
  admin["email"] = req.body.email.toLowerCase().trim();
  admin["password"] = req.body.password.toLowerCase().trim();
  adminModel.findOne({ email: admin["email"] }, (err, result) => {
    if (!err) {
      if (result) {
        bcrypt.compare(admin["password"], result.password, (err, match) => {
          if (err) {
            res.json({ message: "Error password", err });
          } else if (match) {
            let adminJson = {
              id: result._id,
              email: result.email,
              firstName: result.firstName
            };
            const token = jwt.sign(adminJson, "secret_key_here");
            var response = {
              token: token,
              userData: adminJson
            };
            adminModel.findOneAndUpdate(
              {
                email: admin["email"]
              },
              { token: token },
              (err, finalResult) => {
                if (err) throw err;
                console.log(
                  "successfully updated token in database ",
                  finalResult
                );
                res.status(200).json({ status: 200, response });
              }
            );
          } else {
            res
              .status(401)
              .json({ status: 401, message: "Password did not match" });
          }
        });
      } else {
        res.status(404).json({ status: 404, message: "Email not found" });
      }
    } else {
      res.status(401).json({ message: "Invalid Credentials" });
    }
  });
};

var registerAdmin = (req, res) => {
  // checking the required payload
  console.log("you are here", req.body);
  req.checkBody("email", "Please send the email").exists();
  req.checkBody("password", "Please send the password").exists();
  var admin = new Object(); // creating an instance of the model for saving
  // Alloting values to the instance
  var errorMessage = "";
  admin.email = req.body.email ? req.body.email : "";
  admin.password = req.body.password
    ? req.body.password.toLowerCase().trim()
    : "";
  // firing the mongoose query for saving user
  adminModel(admin).save((err, data) => {
    if (err) {
      console.log("Error While saving user ", err);
      switch (err.name) {
        case "ValidationError":
          for (field in err.errors) {
            if (errorMessage == "") {
              errorMessage = err.errors[field].message;
            } else {
              errorMessage += ", " + err.errors[field].message;
            }
          } //for
          break;
      } //switch
      console.log("errorMessage---------" + errorMessage);
      res.status(401).json({ status: 401, message: errorMessage });
    } else {
      console.log("data saved successfully saved ", data);
      res.status(200).json({ status: 200, message: "saved user" });
    }
  });
};

module.exports = {
  adminLogin: adminLogin,
  registerAdmin: registerAdmin
};
