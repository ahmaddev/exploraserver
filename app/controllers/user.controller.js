var userModel = require("../models/user");
var bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");
var authenticate = (req, res) => {
  req.checkBody("email", "Please send email").exists();
  req.checkBody("password", "Please send password ").exists();
  var errors = req.validationErrors();
  if (errors) {
    console.log("getting errors" + JSON.stringify(errors));
    res.status(400).json({ errors: errors });
  }
  var user = {};
  user["email"] = req.body.email;
  user["password"] = req.body.password;
  userModel.findOne(
    { email: user["email"], isDeleted: false, isEnabled: true },
    (err, result) => {
      if (!err) {
        if (result) {
          bcrypt.compare(user["password"], result.password, (err, match) => {
            if (err) {
              res.json({ message: "Error password", err });
            } else if (match) {
              let userJson = {
                id: result._id,
                email: result.email,
                name: result.name,
                location: result.location,
                interests: result.interests
              };
              const token = jwt.sign(userJson, "secret_key_here");
              var response = {
                token: token,
                userData: userJson
              };
              userModel.findOneAndUpdate(
                {
                  email: user["email"]
                },
                { token: token },
                (err, finalResult) => {
                  if (err) throw err;
                  console.log(
                    "successfully updated token in database ",
                    finalResult
                  );
                  res.status(200).json({ status: 200, response });
                }
              );
            } else {
              res
                .status(401)
                .json({ status: 401, message: "Password did not match" });
            }
          });
        } else {
          res.status(404).json({ status: 404, message: "Email not found" });
        }
      } else {
        res.status(401).json({ message: "Invalid Credentials" });
      }
    }
  );
};

var register = (req, res) => {
  // checking the required payloads
  req.checkBody("firstName", "Please send the first name").exists();
  req.checkBody("email", "Please send the email").exists();
  req.checkBody("password", "Please send the password").exists();
  var user = new Object(); // creating an instance of the model for saving
  // Alloting values to the instance
  var errorMessage = "";
  user.name = req.body.name ? req.body.name : "";
  user.location = req.body.location ? req.body.location : [];
  user.city = req.body.city ? req.body.city : [];
  user.profileImage = req.body.profileImage ? req.body.profileImage : [];
  user.email = req.body.email ? req.body.email.toLowerCase().trim() : "";
  user.password = req.body.password
    ? req.body.password.toLowerCase().trim()
    : "";
  // firing the mongoose query for saving user
  userModel(user).save((err, data) => {
    if (err) {
      console.log("Error While saving user ", err);
      switch (err.name) {
        case "ValidationError":
          for (field in err.errors) {
            if (errorMessage == "") {
              errorMessage = err.errors[field].message;
            } else {
              errorMessage += ", " + err.errors[field].message;
            }
          } //for
          break;
      } //switch
      console.log("errorMessage---------" + errorMessage);
      res.status(401).json({ status: 401, message: errorMessage });
    } else {
      let userJson = {
        id: data._id,
        email: req.body.email,
        name: req.body.name,
        location: req.body.location
      };
      const token = jwt.sign(userJson, "secret_key_here");
      var response = {
        token: token,
        userData: userJson
      };
      console.log("data saved successfully saved ", response);
      res.status(200).json({ status: 200, message: "saved user", response });
    }
  });
};

var getUsers = (req, res) => {
  userModel.find({ isDeleted: false, isEnabled: true }, (err, data) => {
    if (err) throw err;
    else {
      if (data.length) {
        res.json({ status: 200, message: "you are getting users", data });
      } else {
        res.json({ status: 200, message: "No users" });
      }
    }
  });
};

var editSingleUser = (req, res) => {
  req.checkParams("id", "student id not received");
  // req.checkBody("name", "first name required");
  // req.checkBody("location", "location required");
  // console.log("these are languages", req.body.language);

  var profiledata = {
    name: req.body.name,
    bio: req.body.bio,
    location: req.body.location
  };

  userModel.findOneAndUpdate(
    { _id: req.params.id },
    profiledata,
    (err, data) => {
      if (err) {
        throw err;
      } else {
        console.log("this is the update data", data);
        res.json({ status: 200, message: "data update", data });
      }
    }
  );
};
var addUserInterest = (req, res) => {
  req.checkParams("id", "user id not received");
  console.log("GETTING BODY", req.body);
  console.log("getting insterset", req.params.id, req.body);
  var interestData = {
    interests: req.body
  };
  var id = req.params.id ? req.params.id : "";
  if (id) {
    userModel.findByIdAndUpdate(
      { _id: req.params.id },
      interestData,
      (err, data) => {
        if (err) {
          throw err;
        } else {
          console.log("getting updated status", data);
          res.json({ status: 200, message: "interest added", data });
        }
      }
    );
  } else {
    res.status(401).json({ message: "Please send user id" });
  }
};
var addUserImages = (req, res) => {
  req.checkParams("id", "teacher id not received");
  req.checkBody("profileImage", "docs not received");
  var id = req.params.id;
  console.log("this is the profile image", req.body.profileImage);
  var profile_base64Data = req.body.profileImage.replace(
    /^data:image\/png;base64,/,
    ""
  );
  userModel.findById(id, (err, data) => {
    if (err) {
      res.send(err);
    } else {
      console.log("first phase data", data);
      if (data.profileImage) {
        console.log("another process started");
        require("fs").unlinkSync(
          "public/teacher/dp/" + data.profileImage,
          (derr, dat) => {
            if (derr) {
              console.log("This is the error ", derr);
            }
            console.log("image removed", dat);
          }
        );
        var complete_image_url =
          id + Math.floor(Math.random() * 600) + 1 + ".png";
        require("fs").writeFile(
          "public/teacher/dp/" + complete_image_url,
          profile_base64Data,
          "base64",
          function(gerr) {
            if (gerr) {
              console.log(
                "error while inserting the profile image for the first time "
              );
            }
            var dtdata = {
              profileImage: complete_image_url
            };
            teacherModel.findByIdAndUpdate(
              id,
              dtdata,
              { new: true },
              (err, data) => {
                if (err) {
                  res.send(err);
                } else {
                  if (data) {
                    res.json({
                      status: 200,
                      message: "teacher doc updated successfully",
                      data
                    });
                  }
                }
              }
            );
          }
        );
      } else {
        var complete_image_url =
          id + Math.floor(Math.random() * 500) + 1 + ".png";
        var ddata = {
          profileImage: complete_image_url
        };
        require("fs").writeFile(
          "public/teacher/dp/" + complete_image_url,
          profile_base64Data,
          "base64",
          function(gerr) {
            if (gerr) {
              console.log(
                "error while inserting the profile image for the first time "
              );
            }
            teacherModel.findByIdAndUpdate(
              id,
              ddata,
              { new: true },
              (err, data) => {
                if (err) {
                  res.send(err);
                } else {
                  if (data) {
                    res.json({
                      status: 200,
                      message: "Teacher docs updated successfully",
                      data
                    });
                  }
                }
              }
            );
          }
        );
      }
    }
  });
};

module.exports = {
  authenticate: authenticate,
  register: register,
  getUsers: getUsers,
  editSingleUser: editSingleUser,
  addUserInterest: addUserInterest,
  addUserImages: addUserImages
};
