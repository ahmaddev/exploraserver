var interestModel = require("../models/interest");

var saveInterest = (req, res) => {
  req.checkBody("interestname", "Please send the interest name ").exists();
  console.log("getting payload " + req.body.interestname);
  var interest = new interestModel();
  interest.interestName = req.body.interestname;
  interest.save((err, user) => {
    if (err) throw err;
    else {
      res.status(200).json({ message: "interest saved" });
    }
  });
};

var getInterests = (req, res) => {
  interestModel
    .find({ isDeleted: false })
    .select({ _id: 1, interestName: 1 })
    .exec((err, data) => {
      if (err) throw err;
      if (data.length) {
        console.log("getting data", data);
        res.status(200).json({ status: 200, message: "getting data", data });
      } else {
        console.log("getting data", data);
        res.status(200).json({ message: "No Data" });
      }
    });
};
var gettingId = (req, res) => {
  interestModel
    .find()
    .sort({ createdAt: -1 })
    .select({ _id: 1, interestName: 1 })
    .exec((err, data) => {
      if (err) throw err;
      if (data.length) {
        console.log("getting data", data);
        res.status(200).json({ status: 200, message: "getting data", data });
      } else {
        console.log("getting data", data);
        res.status(200).json({ message: "No Data" });
      }
    });
};

const deleteInterest = (req, res) => {
  // interestModel.find()
  var id = req.params.id ? req.params.id : "";
  if (id) {
    id = mongoose.Types.ObjectId(req.params.id);

    interestModel
      .findOneAndUpdate({ _id: id }, { isDeleted: false })
      .lean()
      .exec((err, data) => {
        if (err) throw err;
        if (data.isModified != 0) {
          res
            .status(200)
            .json({ status: 200, message: "Deleted Successfully" });
        } else {
          res.status(202).json({ status: 202, message: "Error! not updated" });
        }
      });
  } else {
    res.status(401).json({ status: 401, message: "please send id" });
  }
};

module.exports = {
  saveInterest: saveInterest,
  getInterests: getInterests,
  gettingId: gettingId,
  deleteInterest: deleteInterest
};
