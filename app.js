var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
var validator = require("express-validator");
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var interestRouter = require("./routes/interest");
var adminRouter = require("./routes/admin");
var activityRouter = require("./routes/activity");

var app = express();
var db = require("./config/db");
var apiRoutes = express.Router();
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());
// app.use("/", indexRouter);
app.use(validator());

// apiRoutes.use((req, res, next) => {
//   var token = req.body.token || req.query.token || req.headers["x-auth-token"];
//   if (token) {
//     next();
//   } else {
//     return res.status(403).json({
//       status: 403,
//       message: "No token provided"
//     });
//   }
// });
app.use("/users", usersRouter);
app.use("/int", interestRouter);
app.use("/ad", adminRouter);
app.use("/act", activityRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
