var mongoose = require("mongoose");

// mongoose.connect("mongodb://localhost:27017/explora");
mongoose.connect(
  "mongodb://explora:explora123@ds151805.mlab.com:51805/exploradb"
);

var db = mongoose.connection;

db.on("open", err => {
  if (!err) {
    console.log("Database Connected");
  } else {
    console.log("Database Not Connected" + err);
  }
});
